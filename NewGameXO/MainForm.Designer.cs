﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 26.11.2018
 * Time: 18:18
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace NewGameXO
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.PictureBox paintArea;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.paintArea = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize) (this.paintArea)).BeginInit();
			this.SuspendLayout();
			// 
			// paintArea
			// 
			this.paintArea.Location = new System.Drawing.Point(12, 7);
			this.paintArea.MaximumSize = new System.Drawing.Size(600, 600);
			this.paintArea.MinimumSize = new System.Drawing.Size(600, 600);
			this.paintArea.Name = "paintArea";
			this.paintArea.Size = new System.Drawing.Size(600, 600);
			this.paintArea.TabIndex = 0;
			this.paintArea.TabStop = false;
			this.paintArea.Paint += new System.Windows.Forms.PaintEventHandler(this.PaintAreaPaint);
			this.paintArea.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PaintAreaMouseClick);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(634, 611);
			this.Controls.Add(this.paintArea);
			this.MaximumSize = new System.Drawing.Size(650, 650);
			this.MinimumSize = new System.Drawing.Size(650, 650);
			this.Name = "MainForm";
			this.Text = "NewGameXO";
			this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MainFormMouseClick);
			((System.ComponentModel.ISupportInitialize) (this.paintArea)).EndInit();
			this.ResumeLayout(false);
		}

		}
	}