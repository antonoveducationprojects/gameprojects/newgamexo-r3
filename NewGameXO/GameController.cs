﻿/*
 * Created by SharpDevelop.
 * User: guyver
 * Date: 27.11.2018
 * Time: 8:29
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;

namespace NewGameXO
{
	/// <summary>
	/// Description of ......
	/// </summary>
	public class GameController
	{
		int[,] GameMap= new int[3,3];
		protected int currentStep;
		protected const int codeX=1,codeO=0,codeFree=-1;
		protected Graphics gDev;
		protected GameFild gameFild;
		protected AbstractGameFigure[] figures=new AbstractGameFigure[2];
		public GameController(Graphics dev)
		{
			gDev=dev;
			currentStep=0;
			gameFild=new GameFild();
			figures[0]=new SimpleXFigure();
			figures[1]=new SimpleOFigure();
			ResetMap();
		}
		protected void ResetMap()
		{
			for(int i=0;i<3;++i)
				for(int j=0;j<3;++j)
					GameMap[i,j]=codeFree;
		}
		public void DisplayMap(Graphics gDev)
		{
			int figureCode;
			gameFild.Display(gDev);
			for(int i=0;i<3;++i)
				for(int j=0;j<3;++j)
			    {
					figureCode=GameMap[i,j];				
					if(figureCode==codeFree)
						continue;
					figures[figureCode].Display(gDev,i*200,j*200);
				}

		}
		public void MakeJob(int x, int y)
		{
			int i=x/200;
			int j=y/200;
			if(GameMap[i,j]!=codeFree)
				return;
			GameMap[i,j]=currentStep%2;
			currentStep++;
		}
	}
}
